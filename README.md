#  Bank Scraper

Bank Scraper é um script que permite recuperar algumas informaçōes de alguns bancos brasileiros.

**Executar esse script por muitas vezes seguidas poderá bloquear sua conta.**
### Bancos Suportados

  - Nubank (Necessário autenticação com QRCode)

# Pré-Requisitos
  - [NodeJS LTS](https://nodejs.org/en/)
  - [MongoDB Stable](https://www.mongodb.com/download-center/community)
  
**OU**

   - [Docker](https://www.docker.com/)

# Como rodar
Docker
```sh
docker-compose up
```

Node
```sh
npm install
npm run build
node dist/main/index.js
```

Desenvolvimento (Watch)
```sh
npm run watch
```

Testes (watch)
```sh
npm run watch-test
```

Testes
```sh
npm run test
```

Build
```sh
npm run build
```

Lint
```sh
npm run tslint
```