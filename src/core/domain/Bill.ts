export enum BillStatus {
    OPEN = 'open',
    CLOSED = 'closed',
    OVERDUE = 'overdue',
}

export class Bill {
    id: string;
    status: BillStatus;
    dueDate: Date;
    closeDate: Date;
    totalBalance: number;
    interestRate: string;
    interest: number;
    paid: boolean;
    minimumPayment: number;
    createdAt: Date;

    constructor(bill: any){
        this.id = bill.id;
        this.status = bill.status;
        this.dueDate = bill.dueDate;
        this.closeDate = bill.closeDate;
        this.totalBalance = bill.totalBalance;
        this.interestRate = bill.interestRate;
        this.interest = bill.interest;
        this.paid = bill.paid;
        this.minimumPayment = bill.minimumPayment;
        this.createdAt = bill.createdAt;
    }

}