import { IUseCase } from '../use-case.interface';
import { IBankScraper } from '../../services/bank-scraper/scraper.interface';

interface Input {
    login: string;
    password: string;
}

interface Output {
    qrCode: string;
    uuid: string;
}

export class Authentication implements IUseCase<Input, Promise<Output>> {
    private scraper: IBankScraper;

    constructor(scraper: IBankScraper) {
        this.scraper = scraper;
    }

    async execute(input: Input): Promise<Output> {
        return this.scraper.authenticate(input.login, input.password);
    }

}