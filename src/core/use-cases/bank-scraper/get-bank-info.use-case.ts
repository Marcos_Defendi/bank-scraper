import { IUseCase } from '../use-case.interface';
import { IBankScraper } from '../../services/bank-scraper/scraper.interface';
import { Bill } from '../../domain/Bill';
import { IBankParser } from '../../services/bank-scraper/parser.interface';
import { IBillsRepository } from '../../repositories/bills.repository.interface';

interface Input {
   login: string;
}

export class GetBankInfo implements IUseCase<Input, Promise<Bill[]>> {
    private scraper: IBankScraper;
    private parser: IBankParser;
    private billsRepository: IBillsRepository;

    constructor(scraper: IBankScraper, parser: IBankParser, billsRepository: IBillsRepository){
        this.scraper = scraper;
        this.parser = parser;
        this.billsRepository = billsRepository;
    }

    async execute(): Promise<Bill[]> {
        const bills = this.parser.parse(await this.scraper.getInfo());
        this.billsRepository.insertMany(bills);
        return bills;
    }

}