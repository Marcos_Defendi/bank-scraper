import { Bill } from '../../domain/Bill';

export interface IBankParser {
    parse(input: any): Bill[];
}