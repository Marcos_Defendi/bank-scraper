import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import expressLayouts from 'express-ejs-layouts';
import { GetBankInfo, Authentication } from '../core';
import { NubankScraper, NubankParser, DatabaseFactory, MongoDbBillRepository } from '../infra';

const app = express();
const port = 8080;

app.set('views', path.resolve('src', 'presentation', 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(bodyParser.urlencoded({ extended: true }));

const nubankScraper = new NubankScraper();
const nubankParser = new NubankParser();

const getInfo = new GetBankInfo(nubankScraper, nubankParser, new MongoDbBillRepository());
const auth = new Authentication(nubankScraper);
DatabaseFactory.init();

app.get('/', ( req, res ) => {
    res.render('form');
});

app.post('/qrcode', async (req, res) => {
    const { login, password } = req.body as { [key: string]: string };
    const {qrCode} = await auth.execute({ login, password });

    res.render('qrcode', { qrCode })
});

app.post('/balance', async (req, res) => {
    const bills = await getInfo.execute();

    res.render('balance', { bills })
});

app.listen(port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );