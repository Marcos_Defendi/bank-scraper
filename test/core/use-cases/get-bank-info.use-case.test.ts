import { GetBankInfo } from '../../../src/core/use-cases';

describe('GetBankInfo Use Case', () => {
    const scraper = {
        authenticate: jest.fn(),
        getInfo: jest.fn().mockResolvedValue([]),
    };

    const parser = {
        parse: jest.fn().mockReturnValue([]),
    };

    const repository = {
        insertMany: jest.fn(),
    };

    const useCase = new GetBankInfo(scraper, parser, repository);

    it('should call the parser.parse method with the return of scraper method', async() => {
        await useCase.execute();
        expect(parser.parse).toBeCalledWith([]);
    });

    it('should call the repository.insertMany method with the return of parser method', async() => {
        await useCase.execute();
        expect(repository.insertMany).toBeCalledWith([]);
    });

    it('should return the return of parser.parse method', async() => {
        expect(await useCase.execute()).toEqual([]);
    });
});