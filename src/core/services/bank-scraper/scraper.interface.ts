import { Bill } from '../../domain/Bill';

export interface IBankScraper {
    authenticate(login: string, password: string): Promise<{ qrCode: string, uuid: string }>;
    getInfo(): Promise<any[]>
}