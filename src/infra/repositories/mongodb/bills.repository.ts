import { IBillsRepository } from '../../../core/repositories/bills.repository.interface';
import { Bill } from '../../../core/domain/Bill';
import { Schema, model as MongooseModel, Document } from 'mongoose';
import { MongoDbBaseRepository } from './base.repository';

const billSchema = new Schema({
    id: String,
    status: String,
    dueDate: Date,
    closeDate: Date,
    totalBalance: Number,
    interestRate: String,
    interest: Number,
    paid: Boolean,
    minimumPayment: Number,
    createdAt: Date,
});

const Model = MongooseModel('Bills', billSchema);

export class MongoDbBillRepository extends MongoDbBaseRepository<Bill> implements IBillsRepository {
    constructor() {
        super(Model);
    }

}