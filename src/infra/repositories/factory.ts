import mongoose from 'mongoose';

export class DatabaseFactory {

    private constructor() { }

    static init(): void {
        mongoose.connect('mongodb://db:27017/bank-scraper', { useNewUrlParser: true, useUnifiedTopology: true });
    }
}