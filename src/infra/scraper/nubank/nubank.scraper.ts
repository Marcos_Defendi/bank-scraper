import { Nubank, NubankAuthorizer } from 'nubank-client';
import { IBankScraper } from '../../../core/services/bank-scraper/scraper.interface';

export class NubankScraper implements IBankScraper {
    private nubankAuthorizer: NubankAuthorizer;
    private uuid: string;
    private accessToken: string;

    constructor() {
        this.nubankAuthorizer = new NubankAuthorizer();
    }

    async authenticate(login: string, password: string): Promise<any> {
        const { access_token } = await this.nubankAuthorizer.login(login, password);
        this.accessToken = access_token;
        const liftId = await this.nubankAuthorizer.getLiftId();
        this.uuid = liftId.uuid;
        return liftId;
    }

    async getInfo(): Promise<any[]> {
        const liftResponse = await this.nubankAuthorizer.lift(this.uuid, this.accessToken, 15);
        const nubank = new Nubank(liftResponse);
        const bills = await nubank.getBills();
        return bills.getSummary().bills;
    }

}

