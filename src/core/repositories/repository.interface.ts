export interface IWriteOptions {
  multi?: boolean;
}

export interface IWrite<T> {
  insertMany(item: T[]): Promise<any>;
}