FROM node:10

WORKDIR /app

COPY . .

RUN npm install --production
RUN npm run build

EXPOSE 8080
