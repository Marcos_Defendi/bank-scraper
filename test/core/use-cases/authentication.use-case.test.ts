import { Authentication } from '../../../src/core/use-cases';

describe('Authentication Use Case', () => {
    const scraper = {
        authenticate: jest.fn().mockResolvedValue({ qrCode: 'qrCode', uuid: 'uuid' }),
        getInfo: jest.fn(),
    };

    const useCase = new Authentication(scraper);

    it('should call the scraper.authenticate method with the correct parameter', () => {
        useCase.execute({ login: 'test', password: 'test' });
        expect(scraper.authenticate).toBeCalledWith('test', 'test');
    });

    it('should return the return of parser.parse method', async () => {
        expect(await useCase.execute({ login: 'test', password: 'test' })).toEqual({ qrCode: 'qrCode', uuid: 'uuid' });
    });
});