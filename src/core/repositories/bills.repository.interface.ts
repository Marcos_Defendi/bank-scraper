import { IWrite } from './repository.interface';
import { Bill } from '../domain/Bill';

export interface IBillsRepository extends IWrite<Bill>{
}