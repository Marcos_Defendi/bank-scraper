import mongoose from 'mongoose';
import { IWrite } from '../../../core/repositories/repository.interface';

export abstract class MongoDbBaseRepository<T> implements IWrite<T>{
    private model: mongoose.Model<mongoose.Document>;

    protected constructor(schemaModel: mongoose.Model<mongoose.Document>){
        this.model = schemaModel;
    }

    async insertMany(items: T[]): Promise<any> {
        return this.model.insertMany(items);
    }

}