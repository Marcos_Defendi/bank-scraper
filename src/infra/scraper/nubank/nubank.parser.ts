import { IBankParser } from '../../../core/services/bank-scraper/parser.interface';
import { Bill, BillStatus } from '../../../core/domain/Bill';

export class NubankParser implements IBankParser {

    private getStatus(state: string): BillStatus {
        if (state === 'future' || state === 'open') {
            return BillStatus.OPEN
        }
        return state === 'closed' ? BillStatus.CLOSED : BillStatus.OVERDUE;
    }

    parse(input: any[]): Bill[] {
        return input.map((bill) => new Bill({
            id: bill.id,
            status: this.getStatus(bill.state),
            dueDate: new Date(bill.summary.due_date),
            closeDate: new Date(bill.summary.close_date),
            totalBalance: parseInt(bill.summary.total_balance),
            interestRate: bill.summary.intereset_rate,
            interest: parseInt(bill.summary.interest),
            paid: Boolean(bill.summary.paid),
            minimumPayment: parseInt(bill.summary.minimum_payment),
            createdAt: new Date(bill.summary.open_date),
        }))
    }

}